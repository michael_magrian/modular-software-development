module.exports = function(grunt) {

    // Project configuration
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        path: {
            assets: 'source/assets',
			components: 'source/components',
			scripts: 'source/scripts',
			styles: 'source/styles',
			pub: 'public',
			dist: 'public/dist'
		},

        // Configure the "sass" task
        sass: {
            default: {
                options: {
                    sourceMap: false
                },
                src: '<%= path.styles %>/main.scss',
                dest: '<%= path.dist %>/css/main.css'
            },
			fractal: {
				src: '<%= path.styles %>/fractal.scss',
				dest: '<%= path.pub %>/fractal.css'
			}
        },

        // Configure the "ts" task
        ts: {
            default: {
                options: {
                    target: 'es5',
                    allowJs: true,
                    removeComments: false,
                    sourceMap: false
                },
                src: '<%= path.scripts %>/main.ts',
                dest: '<%= path.dist %>/js/main.js'
            }
        },


		// Configure the "clean" task
		clean: {
			public: ['<%= path.pub %>/*'],
			assets: ['<%= path.dist %>/assets/']
		},


		// Configure the "copy" task
        copy: {
            manifest: {
                expand: true,
                src: 'manifest.json',
                dest: '<%= path.pub %>/'
            },
			assets: {
				expand: true,
				cwd: '<%= path.assets %>/',
				src: '**/*',
				dest: '<%= path.dist %>/assets/'
			}
        },

        // Configure the "watch" task
        watch: {
            styles: {
                files: [
                    '<%= path.components %>/**/*.scss',
                    '<%= path.styles %>/**/*.scss',
                    '<%= path.styles %>/main.scss'
                ],
                tasks: ['sass']
            },
            scripts: {
                files: [
                    '<%= path.components %>/**/*.ts',
                    '<%= path.scripts %>/**/*.ts',
                    '<%= path.scripts %>/main.ts'
                ],
                tasks: ['ts']
            },
            assets: {
				files: [
					'<%= path.assets %>/**/*'
				],
				tasks: ['clean:assets', 'copy']
            }
        }
    });

    // Load the plugin that provides the "sass" task
    grunt.loadNpmTasks('grunt-sass');

    // Load the plugin that provides the "ts" task
    grunt.loadNpmTasks('grunt-ts');

	// Load the plugin that provides the "clean" task
	grunt.loadNpmTasks('grunt-contrib-clean');

	// Load the plugin that provides the "copy" task
	grunt.loadNpmTasks('grunt-contrib-copy');

    // Load the plugin that provides the "watch" task
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Define default task(s)
    grunt.registerTask('default', ['sass', 'ts', 'copy', 'watch']);

    // Define build task(s)
    grunt.registerTask('build', ['clean:public', 'sass', 'ts', 'copy']);

    // Define dist task(s)
    grunt.registerTask('dist', ['clean:public', 'sass', 'fscss', 'ts', 'copy']);

};