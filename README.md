# README #

### How do I get set up? ###

Make sure the following packages are already installed on your computer: 

* yarn (npm install -g yarn)
* fractal-cli (npm i -g @frctl/fractal or yarn global add @frctl/fractal)
* grunt-cli (npm i -g grunt-cli or yarn global add grunt-cli)

Afterwards you can clone the repository and execute the following terminal/cmd commands in the project's root folder:

* yarn init-project
* yarn build
* yarn dev (starts the development process)

There is no fully functional export task implemented yet