'use strict';

/*
* Require the path module
*/
const path = require('path');

/*
 * Require the Fractal module
 */
const fractal = module.exports = require('@frctl/fractal').create();

const mandelbrot = require('@frctl/mandelbrot')({
	styles: ['default', '/fractal.css'],
	static: {
		mount: 'fractal'
	}
});

fractal.web.theme(mandelbrot);

/*
* Give your project a title.
*/
fractal.set('project.title', 'Modular Software Dev');

// Tell Fractal where the components will live
fractal.components.set('path', __dirname + '/source/components');

// Tell Fractal where the documentation will live
fractal.docs.set('path', __dirname + '/source/docs');

// Tell Fractal where the static assets will live
fractal.web.set('static.path', __dirname + '/public');

// Tell Fractal where the static HTML export should be generated
fractal.web.set('builder.dest', __dirname + '/export');

/*
 * Adding Fractal component statuses
 */
fractal.components.set('statuses', {
    wip: {
        label: 'WIP',
        description: 'Work in progress',
        color: '#ff0063'
    },
    qa: {
        label: 'QA',
        description: 'Waiting for QA approval',
        color: '#ff7700',
    },
    ready: {
        label: 'Ready',
        description: 'Ready to be implemented',
        color: '#29cc29'
    }
})

/*
 * Setting Fractal components default status
 */
fractal.components.set('default.status', 'wip')