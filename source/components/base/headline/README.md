The headline base component uses the HTML-Tags:

 * h1
 * h2
 * h3

It is set up with three modifiers fo the size: 

 * headline--small (28px)
 * headline--medium (42px)
 * headline--large (54px)

In addition to sizing there are two modifiers to center the headline or underline it:

 * headline--centered
 * headline--underlined