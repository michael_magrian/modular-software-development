---
title: Übersicht
---

Bei diesem Projekt handelt es sich um einen Prototyp, entwickelt für die Veranstaltung Modulare Softwareentwicklung im Frontend an der Hochschule Furtwangen. Dozent der Veranstaltung ist Dirk Benkert. Autor des Inhalts ist Michael Magrian