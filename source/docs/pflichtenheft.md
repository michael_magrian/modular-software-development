---
title: Pflichtenheft
---

<h2>1. Projektidee</h2>
<p>Ziel dieses Projektes ist es ein Frontendtemplate für eine Portfoliowebseite zu entwickeln</p>

<hr>
<hr>

<h2>2. Anforderungen</h2>
<ul>
    <li>Startseite</li>
</ul>

<hr>
<hr>

<h2>3. Browser-Unterstützung</h2>
<p>Die Webseite muss den Browser Chrome ab Version 57 unterstützen, sowohl Desktop als auch mobil.</p>

<hr>
<hr>

<h2>4. Namenskonventionen</h2>
<p>Für die Benennung der Elemente werden ausschließlich englische Begriffe genutzt.</p>
<p>IDs und absteigende Selektoren werden vermieden und lediglich kaskadierende Klassennamen und Modifiern nach folgendem Schema genutzt:</p>
<p>
    Root-Klasse: <b>nav</b></br>
    Verschachteltes Element, abgetrennt durch zwei Unterstriche: <b>nav__list</b>
    <hr>
    Root-Klasse: <b>button</b></br>
    Modifier-Klasse, abgetrennt durch zwei Bindestriche: <b>button--active</b>
</p>

<hr>
<hr>

<h2>5. Deliverables</h2>
<ul>
    <li>Entwicklungsumgebung, mit darin befindlichen Dateien zur Erzeugung von HTML- / JavaScript- / CSS-Code</li>
    <li>Beschreibung zur Installation der Entwicklungsumgebung anhand der Bitbucket Readme</li>
    <li>Living-Styleguide-System basierend auf dem Fractal-cli</li>
</ul>
