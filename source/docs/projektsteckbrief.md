---
title: Projektsteckbrief
---

<h2>Projektidee</h2>
<p>Es soll eine Portfoliowebseite für einen Developer entwickelt werden mit der Ubersicht über bereits absolvierte Projekte.</p>

<hr>
<hr>

<h2>Projektzeitraum</h2>
<p>09.10.2017 – 15.01.2018</p>

<hr>
<hr>

<h2>Anforderungen</h2>
<ul>
    <li>Projektteaser</li>
    <li>Kontaktseite</li>
</ul>

<hr>
<hr>

<h2>Autor</h2>
<p>Michael Magrian</p>
