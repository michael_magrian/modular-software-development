module Prjct {
	export class Toggle {
        public init() {
            let toggleList = document.querySelectorAll('.toggle')
            
            for (let i = 0; i < toggleList.length; i++) {
                toggleList[i].addEventListener('click', () => {
                    toggleList[i].classList.toggle('toggle--active')
                })
            }
        }
    }
}
