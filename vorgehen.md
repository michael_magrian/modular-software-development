Global Installation: 

* yarn
* fractal-cli
* grunt-cli

Installation:

* fractal
* sass
* grunt
* grunt-sass
* grunt-watch
* grunt-uglify
* grunt-copy
* grunt-jshint
* grunt-clean

Extending dev-environment capabilities: 

* changing default fractal structue to incorporate source folder for components and docs
* Adding export destination
* Adding Tasks for sass, watch
* Changing default uglify task
* Adding component statuses to visualize current component status within development cycle (wip, qa, ready)
* Adding 'concurrently' package to execute fractal and grunt simultaniously (MAYBE OPTIONAL DUE TO USAGE OF A | (PIPE) TO CONCATENATE TASKS???)